using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays_GUI
{
    public partial class arrays : Form
    {
        private char LF = (char)10;
        private char TAB = (char)09;

        public arrays()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_output.Clear();
        }

        // Eindimensionaler Integer-Array mit 10 Elementen
        private void cmd_int_array_10_Click(object sender, EventArgs e)
        {
            txt_output.Clear();
            
            int[] zahlen = new int[10];                         // Array-deklaration
            Random zufall = new Random();                       // Array mit Zufallszahlen füllen
            for (int i = 0; i < 10; i = i + 1)                  // Zehn Zufallszahlen erzeugen
                zahlen[i] = zufall.Next(0, 101);                // aus dem Zahlenbereich von 0 bis 100
            for (int i = 0; i < 10; i = i + 1)                  // Array ausgeben
                txt_output.Text += zahlen[i].ToString() + LF;
        }

        private void cmd_init_double_5_array_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            double[] zahlen = {-4.6, 0, 13, -45, 2345};         // Array definieren
            /*
            Array ausgeben in einer foreach-Schleife
            Parameter: Datentyp und logischer Name und das Quellenelement
            */
            foreach (double d in zahlen)
            {
                txt_output.Text += d;                       
                txt_output.Text += LF;
            }
        }

        // String-Array mit 3 Elementen
        private void cmd_3_elemente_string_array_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            string[] string3 = new string[3];
            string3[0] = "Ich ";
            string3[1] = "hasse ";
            string3[2] = "C# !!!";

            // Alternative Arraydefinition
            // string3[] satz = {"Ich ","hasse ","C# !!!"}

            // String-Verkettung
            txt_output.Text = string3[0] + string3[1] + string3[2];

            // Alternative Ausgabe mittels Schleife
            /*
            for(int i = 0; i < 3; i = i + 1)
                txt_output.Text += string3[i];
            */
        }

        // String Array splitten
        private void cmd_split_array_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            string adresse = "Hans;Dampf;Rauchweg 13;12345;Qualmstadt";
            string[] hilfsarray;

            // String splitten und in Hilfsarray schreiben
            hilfsarray = adresse.Split(';');
            txt_output.Text += hilfsarray[0] + " " + hilfsarray[1] + LF;
            txt_output.Text += hilfsarray[2] + LF + LF;
            txt_output.Text += hilfsarray[3] + " " + hilfsarray[4];
        }

        // Char-Array
        private void cmd_char_array_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            // Array-Definition
            char[] chararray = { 'H', 'a', 'l', 'l', 'o', '!' };

            // Array ais Wort ausgeben
            for (int i = 0; i < chararray.Length; i = i + 1)
                txt_output.Text += chararray[i].ToString();
        }

        // Char-Array OHNE Satzzeichen rückwärts ausgeben
        private void cmd_char_array_backward_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            char[] chararray = { 'H', 'a', 'l', 'l', 'o', '!' };

            /*
             * array.Length => 6
             * for(int i = chararray.Length - 2; i >= 0; i = i - 1;
             *     txt_output.Text += chararray[i].ToString();
             */

            for (int i = 4; i >= 0; i = i - 1)
                txt_output.Text += chararray[i].ToString();

        }

        // Zweidimensionaler Integer-Array
        private void cmd_2d_integer_array_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            // Array definieren
            int[,] zwei_d_array = new int[3, 5] { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };
            for (int i = 0; i < 3; i = i +1 )
            {
                for (int j = 0; j < 5; j = j + 1)
                    txt_output.Text += zwei_d_array[i, j].ToString() + TAB;
                txt_output.Text += LF;
            }
        }

        // Den Wert 13 aus dem zweidimensionalem Array ausgeben lassen
        private void cmd_wert_13_aus_array_ausgeben_Click(object sender, EventArgs e)
        {
            txt_output.Clear();

            // Array definieren
            int[,] zwei_d_array = new int[3, 5] { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };
            // bestimmtes Element ((Zeile3, Spalte 3) ausgeben lassen
            txt_output.Text = zwei_d_array[2,2].ToString();
        }
    }
}
