namespace Arrays_GUI
{
    partial class arrays
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_output = new System.Windows.Forms.RichTextBox();
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_int_array_10 = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_init_double_5_array = new System.Windows.Forms.Button();
            this.cmd_3_elemente_string_array = new System.Windows.Forms.Button();
            this.cmd_split_string = new System.Windows.Forms.Button();
            this.cmd_char_array = new System.Windows.Forms.Button();
            this.cmd_char_array_backward = new System.Windows.Forms.Button();
            this.cmd_2d_integer_array = new System.Windows.Forms.Button();
            this.cmd_wert_13_aus_array_ausgeben = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(240, 30);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(437, 187);
            this.txt_output.TabIndex = 0;
            this.txt_output.Text = "";
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(581, 393);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(96, 36);
            this.cmd_end.TabIndex = 1;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_int_array_10
            // 
            this.cmd_int_array_10.Location = new System.Drawing.Point(29, 30);
            this.cmd_int_array_10.Name = "cmd_int_array_10";
            this.cmd_int_array_10.Size = new System.Drawing.Size(184, 44);
            this.cmd_int_array_10.TabIndex = 2;
            this.cmd_int_array_10.Text = "Integer Array mit 10 Elementen";
            this.cmd_int_array_10.UseVisualStyleBackColor = true;
            this.cmd_int_array_10.Click += new System.EventHandler(this.cmd_int_array_10_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(432, 393);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(94, 36);
            this.cmd_clear.TabIndex = 3;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_init_double_5_array
            // 
            this.cmd_init_double_5_array.Location = new System.Drawing.Point(29, 93);
            this.cmd_init_double_5_array.Name = "cmd_init_double_5_array";
            this.cmd_init_double_5_array.Size = new System.Drawing.Size(184, 44);
            this.cmd_init_double_5_array.TabIndex = 4;
            this.cmd_init_double_5_array.Text = "Initialisierter Double-Array mit 5 Elementen";
            this.cmd_init_double_5_array.UseVisualStyleBackColor = true;
            this.cmd_init_double_5_array.Click += new System.EventHandler(this.cmd_init_double_5_array_Click);
            // 
            // cmd_3_elemente_string_array
            // 
            this.cmd_3_elemente_string_array.Location = new System.Drawing.Point(29, 158);
            this.cmd_3_elemente_string_array.Name = "cmd_3_elemente_string_array";
            this.cmd_3_elemente_string_array.Size = new System.Drawing.Size(184, 44);
            this.cmd_3_elemente_string_array.TabIndex = 5;
            this.cmd_3_elemente_string_array.Text = "String-Array mit 3 Elementen";
            this.cmd_3_elemente_string_array.UseVisualStyleBackColor = true;
            this.cmd_3_elemente_string_array.Click += new System.EventHandler(this.cmd_3_elemente_string_array_Click);
            // 
            // cmd_split_string
            // 
            this.cmd_split_string.Location = new System.Drawing.Point(29, 225);
            this.cmd_split_string.Name = "cmd_split_string";
            this.cmd_split_string.Size = new System.Drawing.Size(184, 46);
            this.cmd_split_string.TabIndex = 6;
            this.cmd_split_string.Text = "String splitten";
            this.cmd_split_string.UseVisualStyleBackColor = true;
            this.cmd_split_string.Click += new System.EventHandler(this.cmd_split_array_Click);
            // 
            // cmd_char_array
            // 
            this.cmd_char_array.Location = new System.Drawing.Point(29, 294);
            this.cmd_char_array.Name = "cmd_char_array";
            this.cmd_char_array.Size = new System.Drawing.Size(184, 45);
            this.cmd_char_array.TabIndex = 7;
            this.cmd_char_array.Text = "Char-Array";
            this.cmd_char_array.UseVisualStyleBackColor = true;
            this.cmd_char_array.Click += new System.EventHandler(this.cmd_char_array_Click);
            // 
            // cmd_char_array_backward
            // 
            this.cmd_char_array_backward.Location = new System.Drawing.Point(29, 358);
            this.cmd_char_array_backward.Name = "cmd_char_array_backward";
            this.cmd_char_array_backward.Size = new System.Drawing.Size(184, 45);
            this.cmd_char_array_backward.TabIndex = 8;
            this.cmd_char_array_backward.Text = "Char-Array Rückwärts";
            this.cmd_char_array_backward.UseVisualStyleBackColor = true;
            this.cmd_char_array_backward.Click += new System.EventHandler(this.cmd_char_array_backward_Click);
            // 
            // cmd_2d_integer_array
            // 
            this.cmd_2d_integer_array.Location = new System.Drawing.Point(240, 226);
            this.cmd_2d_integer_array.Name = "cmd_2d_integer_array";
            this.cmd_2d_integer_array.Size = new System.Drawing.Size(168, 45);
            this.cmd_2d_integer_array.TabIndex = 9;
            this.cmd_2d_integer_array.Text = "Zweidimensionaler Integer-Array";
            this.cmd_2d_integer_array.UseVisualStyleBackColor = true;
            this.cmd_2d_integer_array.Click += new System.EventHandler(this.cmd_2d_integer_array_Click);
            // 
            // cmd_wert_13_aus_array_ausgeben
            // 
            this.cmd_wert_13_aus_array_ausgeben.Location = new System.Drawing.Point(240, 294);
            this.cmd_wert_13_aus_array_ausgeben.Name = "cmd_wert_13_aus_array_ausgeben";
            this.cmd_wert_13_aus_array_ausgeben.Size = new System.Drawing.Size(168, 45);
            this.cmd_wert_13_aus_array_ausgeben.TabIndex = 10;
            this.cmd_wert_13_aus_array_ausgeben.Text = "Wert 13 aus Array ausgeben";
            this.cmd_wert_13_aus_array_ausgeben.UseVisualStyleBackColor = true;
            this.cmd_wert_13_aus_array_ausgeben.Click += new System.EventHandler(this.cmd_wert_13_aus_array_ausgeben_Click);
            // 
            // arrays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 464);
            this.Controls.Add(this.cmd_wert_13_aus_array_ausgeben);
            this.Controls.Add(this.cmd_2d_integer_array);
            this.Controls.Add(this.cmd_char_array_backward);
            this.Controls.Add(this.cmd_char_array);
            this.Controls.Add(this.cmd_split_string);
            this.Controls.Add(this.cmd_3_elemente_string_array);
            this.Controls.Add(this.cmd_init_double_5_array);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_int_array_10);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.txt_output);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "arrays";
            this.Text = "Arrays";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txt_output;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_int_array_10;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_init_double_5_array;
        private System.Windows.Forms.Button cmd_3_elemente_string_array;
        private System.Windows.Forms.Button cmd_split_string;
        private System.Windows.Forms.Button cmd_char_array;
        private System.Windows.Forms.Button cmd_char_array_backward;
        private System.Windows.Forms.Button cmd_2d_integer_array;
        private System.Windows.Forms.Button cmd_wert_13_aus_array_ausgeben;
    }
}

